package org.bb.servicegateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutingConfiguration {

	@Bean
	public RouteLocator routes(RouteLocatorBuilder builder) {
		return builder.routes()
		.route("crud-template-mongo", r -> r.path("/crud-template-mongo/**").filters(f -> f.rewritePath("/crud-template-mongo/(?<segment>.*)", "/${segment}")).uri("lb://crud-template-mongo"))
		.route("crud-template-mysql", r -> r.path("/crud-template-mysql/**").filters(f -> f.rewritePath("/crud-template-mysql/(?<segment>.*)", "/${segment}")).uri("lb://crud-template-mysql"))
		.route("crud-template-postgres", r -> r.path("/crud-template-postgres/**").filters(f -> f.rewritePath("/crud-template-postgres/(?<segment>.*)", "/${segment}")).uri("lb://crud-template-postgres"))
		.build();
	}

//	@Bean
//	public ReactiveCircuitBreakerFactory reactiveResilience4JCircuitBreakerFactory(CircuitBreakerRegistry registry) {
//	    ReactiveResilience4JCircuitBreakerFactory factory = new ReactiveResilience4JCircuitBreakerFactory();
//	    factory.configureCircuitBreakerRegistry(registry);
//	    return factory;
//	}
}
